package Test;



import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * This is MapReduce application class, which extract the input CSV
 * file and gives the output as the query CSV file written on HDFS also writes
 * total error count in another file on HDFS.
 * - Pivot for yearly cost of Energy suppliers
 */

public class QueryApplication {
	private String DELIMITER = ",";

	public static class DBUMapper extends Mapper<Object, Text, Text, Text> {
		String tag = "DBU";
		String lineKey = "";
		String lineValue = "";
		private Logger logger = Logger.getLogger(DBUMapper.class);

		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			super.setup(context);
			logger.info("Initializing NoSQL Connection for DBU Mapper.");
		}

		public void map(Object key , Text value, Context context, Reporter reporter) throws IOException, InterruptedException {
			String record = value.toString();
			String[] parts = record.split(",");
			if (parts.length != 14) {
				throw new IOException("Invalid record received");
			}
			logger.info("-+-+KEY: "+key.toString()+"\n-+-+VALUE: "+value.toString());
			// Take Relevant columns:
			// column 6: Provincia (SIGLA)
			this.lineKey = parts[6].trim();
			if(this.lineKey.length()!=2){
				throw new IOException("Invalid key record value received");
			}
			// column 6, 8, 9, 10: Sigla, CF, RAGIONE_SOCIALE (Provider), CAT ABBONAMENTO
			this.lineValue = "DBU\t"+parts[6].trim()+"\t"+parts[8].trim()+"\t"+parts[9].trim()+"\t"+parts[10].trim();

			context.write(new Text(this.lineKey), new Text(this.lineValue));
			//context.collect(new TextPair(this.lineKey, tag), new TextPair(this.lineValue, tag));
		}
	}

	public static class RegioniMapper extends Mapper<Object, Text, Text, Text> {
		String tag = "REGIONI";
		String lineKey = "";
		String lineValue = "";

		private Logger logger = Logger.getLogger(RegioniMapper.class);

		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			super.setup(context);
			logger.info("Initializing NoSQL Connection for Regioni Mapper.");
		}

		public void map(Object key , Text value, Context context, Reporter reporter) throws IOException, InterruptedException {
			String record = value.toString();
			String[] parts = record.split(",");

			logger.info("-+-+KEY: "+key.toString()+"\n-+-+VALUE: "+value.toString());

			if (parts.length != 4) {
				throw new IOException("Invalid record received");
			}
			// Take Relevant columns:
			// column 4: CAP
			this.lineKey = parts[2].trim();
			if(this.lineKey.length()!=2){
				throw new IOException("Invalid key record value received");
			}
			// column tutte: Provincia, Regione, Sigla, Densita Demogafica
			this.lineValue = "RGNXZ\t"+parts[2].trim()+"\t"+parts[0].trim()+"\t"+parts[1].trim()+"\t"+parts[3].trim();

			context.write(new Text(this.lineKey), new Text(this.lineValue));
			//context.collect(new TextPair(this.lineKey, tag), new TextPair(this.lineValue, tag));
		}
	}

	public static class ReducerJoin extends Reducer<Text, Text, Text, NullWritable> {
		private Logger logger = Logger.getLogger(RegioniMapper.class);

		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			super.setup(context);
			logger.info("Initializing NoSQL Connection for JOIN Reducer.");
		}

		public void reduce(Text key, Iterable<Text> values, Context context, Reporter reporter)
				throws IOException, InterruptedException {
			String regionName = "";
			String dbuValue = "";

			logger.info("-+-+KEY: "+key.toString()+"\n");

			for (Text t : values){
				String parts[] = t.toString().split("\t");
				if(parts[0].equals("DBU")){
					dbuValue = parts[1].trim()+"\t"+parts[2].trim()+"\t"+parts[3].trim()+"\t"+parts[4].trim();
				}else if(parts[0].equals("RGNXZ")){
					regionName = parts[2];
				}
				logger.info("\n-+-+VALUE: "+t.toString());
				String finalString = regionName+"\t"+dbuValue;
				context.write(new Text(finalString), NullWritable.get());
			}
//			while(values.hasNext())
//			{
//				value = values.next();
//				context.write(key.getFirst(), new Text(value.toString() + "\t" + value.toString()));
//				for(Text val : T1) {
//					//output.collect(key.getFirst(), new Text(val.toString() + "\t" + value.getFirst().toString()));
//				}
//			}
		}
	}

	public static void main(String[] args) throws Exception {
		if(args.length!=3){
			System.out.print("Run as -- hadoop jar /path/to/artifact.jar /inputdataset1 /inputdataset2 /output");
			System.exit(-1);
		}

		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "CSV Join Stage 2");

		job.setJarByClass(QueryApplication.class);
		//job.setNumReduceTasks(0);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(NullWritable.class);


		// DBU
		MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, DBUMapper.class);
		// Regioni
		MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, RegioniMapper.class);

		//job.setPartitionerClass(CustomPartitioner.class);
		job.setReducerClass(ReducerJoin.class);
		//job.setGroupingComparatorClass(JComparator.class);
		//job.setCombinerClass(CSVQueryReducer.class);

		FileOutputFormat.setOutputPath(job, new Path(args[2]));

		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
