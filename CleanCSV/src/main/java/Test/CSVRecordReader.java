package Test;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.LineRecordReader;
import org.apache.hadoop.mapred.RecordReader;

import java.io.IOException;

public class CSVRecordReader implements RecordReader<Text, Text> {
	private LineRecordReader lineReader;
	private LongWritable lineKey;
	private Text lineValue;
	private int keyColumn;
	private int numColumns;

	public CSVRecordReader(JobConf job, FileSplit split, int keyColumn, int numColumns) throws IOException {
		lineReader = new LineRecordReader(job, split);
		lineKey = lineReader.createKey();
		lineValue = lineReader.createValue();
		this.keyColumn = keyColumn;
		this.numColumns = numColumns;
	}

	@Override
	public boolean next(Text key, Text value) throws IOException {
		// get the next line
		if (!lineReader.next(lineKey, lineValue)) {
			return false;
		}

		// parse the lineValue which is in the format:
		// c1, ... , cN
		String [] pieces = lineValue.toString().split(",");
		if (pieces.length != this.numColumns) {
			throw new IOException("Invalid record received");
		}

		String composeValue = "";
		for (int i=0; i<pieces.length; i++){
			if (i==this.keyColumn){
				key.set(pieces[8].trim());
			} else {
				composeValue = pieces[i].trim()+',';
			}
		}
		if(composeValue.endsWith(",")){
			composeValue = composeValue.substring(0,composeValue.length()-1);
		}
		value.set(composeValue.trim());
		return true;
	}

	@Override
	public Text createKey() {
		return new Text("");
	}

	@Override
	public Text createValue() {
		return new Text("");
	}

	@Override
	public long getPos() throws IOException {
		return lineReader.getPos();
	}

	@Override
	public void close() throws IOException {
		lineReader.close();
	}

	@Override
	public float getProgress() throws IOException {
		return lineReader.getProgress();
	}
}
