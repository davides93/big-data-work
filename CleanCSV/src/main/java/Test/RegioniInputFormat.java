package Test;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;
import java.io.IOException;

public class RegioniInputFormat extends FileInputFormat<Text, Text> {
	public RegioniInputFormat() {
	}

	public RecordReader<Text, Text> getRecordReader(
			InputSplit input, JobConf job, Reporter reporter)
      throws IOException {
			reporter.setStatus(input.toString());
			return new CSVRecordReader(job, (FileSplit)input, 1, 4);
		}
}
