package Test;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/** A WritableComparator optimized for Text keys. */
public class JComparator extends WritableComparator {
	//group by muti-key
	public JComparator() {
		super(TextPair.class, true);
	}

	public int compare(WritableComparable a, WritableComparable b) {
		TextPair t1 = (TextPair) a;
		TextPair t2 = (TextPair) b;
		return t1.getFirst().compareTo(t2.getFirst());
	}
}