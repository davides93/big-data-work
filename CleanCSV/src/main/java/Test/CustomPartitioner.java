package Test;

import org.apache.hadoop.mapreduce.Partitioner;

public class CustomPartitioner extends Partitioner<TextPair, TextPair> {

	@Override
	public int getPartition(TextPair key, TextPair value, int numPartitions) {
//		String name;
//		String parts[] = value.toString().split(",");
//		if(parts[0].equals("DBU")){
//			name = parts[3];
//		}
		return (key.getFirst().hashCode() & Integer.MAX_VALUE) % numPartitions;
	}
}