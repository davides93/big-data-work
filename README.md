# Big Data Work: Hadoop and MapReduce approach

Thesis based on Hadoop MapReduce

## Getting Started

This project will develop some map reduce jobs for performing ETL on Hadoop using the test case discussed in the related paper.

### Prerequisites

For launch the build the jobs module are required:
- JDK 1.8_144
- MAVEN (i recommend to use IntelliJ for automatic sync on MAVEN)
- Cloudera Quickstart on VBox or another CDH Distribution running JDK 1.8_144 and Hadoop


### Listings

The Maven modules related to this work are the following:
```
PreProcessing
AverageAnnualElectricityBill
PivotISPClients
```


### Building

Build artifcat of each module as JAR with IntelliJ or Maven using the following command
```
clean compile package
```

### Setting Up HDFS Environment

Setting up the the working directory on HDFS using the following command (ex. using CDH vm):
```
su hdfs 
# Create Folder
hdfs dfs -mkdir -p /user/cloudera/input/avg
hdfs dfs -mkdir -p /user/cloudera/output/avg
# Copy each dataset into input folder
hdfs dfs -copyFromLocal ./MELIB.csv /user/cloudera/input/avg
```


## Running the tests
Import each artifact Jar into Hadoop Environment and run with the following command
```
-- Stage 1
hadoop jar PreProcessing_jar/PreProcessing.jar MainJob.ReduceJoin -D stage1=yes
-- Stage 2:4
hadoop jar AverageAnnualElectricityBill_jar/AverageAnnualElectricityBill.jar MainJob.ReduceJoin -D stage2=yes
-- Stage 2:3
hadoop jar PivotISPClients_jar/PivotISPClients.jar MainJob.ReduceJoin -D stage2=yes
```


### Break down into end to end tests

Access to Apache Hue for see the result in terms of timing and access to csv output
```
http://localhost:8888
```


## Built With

* [Maven](https://maven.apache.org/) - Dependency Management


## Authors

* **Davide Santoro** - *Initial work*