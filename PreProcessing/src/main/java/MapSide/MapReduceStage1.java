package MapSide;

import MainJob.MYCOUNTER;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MapReduceStage1 {
	public static class CSVCleanMapper extends Mapper<Object, Text, Text, NullWritable> {
		private Text line = new Text();
		private String DELIMITER = ",";

		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			StringBuilder sb = new StringBuilder();
			String[] tokens = value.toString().split(DELIMITER);
			boolean isDifferent = false;
			for (int i = 0; i < tokens.length; i++) {
				String token = tokens[i];
				token = token.trim();
				int previousLength = token.length();
				token = token.replaceAll("[^a-zA-Z0-9]", "");
				if(previousLength>token.length()){
					isDifferent = true;
				}
				sb.append(token);
				if (i < tokens.length - 1)
					sb.append(DELIMITER);
			}
			if (isDifferent)
				context.getCounter(MYCOUNTER.DIRTY_RECORD_COUNT).increment(1);
			else
				context.getCounter(MYCOUNTER.ALREADY_CLEANED_RECORD_COUNT).increment(1);
			line.set(sb.toString());
			context.write(line, NullWritable.get());
		}
	}

	public static class CSVCleanReducer extends Reducer<Text, NullWritable, Text, NullWritable> {
		@Override
		protected void reduce(Text key, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
			context.write(key, NullWritable.get());
		}
	}
}
