package MainJob;

import MapSide.MapReduceStage1;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

public class ReduceJoin extends Configured implements Tool {
	private static String parent_input_path = "/home/hadoop_home/inputs/csv";
	private static String parent_output_path = "/home/hadoop_home/outputs/csv";
	private static String stage1_path = "stage1";
	private static String reduce_side_path = "reduce_side_path";
	private static String stage2_path = "stage2";


	private Configuration conf;
	private boolean stage1=false;
	private boolean reduce_side=false;

	private ReduceJoin() throws IOException {
	}


	private void init(){
		conf = this.getConf();
		try{
			if(conf.get("stage1").compareToIgnoreCase("yes")==0)
				this.stage1=true;
		}catch(NullPointerException e){
			this.stage1=false;
		}
		try{
			if(conf.get("reduceside").compareToIgnoreCase("yes")==0)
				this.reduce_side=true;
		}catch(NullPointerException e){
			this.reduce_side=false;
		}
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new ReduceJoin(), args);
		System.exit(res);
	}

	@Override
	public int run(String[] args) throws Exception {
		init();
		System.out.println("Starting clean up preprocessing - stage 1");
		if(reduce_side)
			run_stage1_reduce_side();
		if(stage1)
			run_stage1();
		return 0;
	}

	/*Pre-Processing: CleanUP the CSV*/
	private int run_stage1() throws InterruptedException, IOException, ClassNotFoundException {
		String input_path = parent_input_path+"/"+stage1_path;
		String output_path = parent_output_path+"/"+stage2_path;
		Path inputPath = new Path(input_path);
		Path outputPath = new Path(output_path);
		FileSystem fs = inputPath.getFileSystem(conf);
		int success = 0;

		conf.setBoolean("mapred.output.compress",true);
		conf.set("mapred.output.compression.type", "BLOCK");
		conf.setClass("mapred.map.output.compression.codec", GzipCodec.class, CompressionCodec.class);

		FileStatus[] fileStatus = fs.listStatus(inputPath);
		for(FileStatus status : fileStatus){
			System.out.println(status.getPath().toString());

			Job job = Job.getInstance(conf, "CSV Cleaner Stage 1: "+status.getPath().getName());

			job.setJarByClass(ReduceJoin.class);
			job.setMapperClass(MapReduceStage1.CSVCleanMapper.class);
			//job.setCombinerClass(MapReduceStage1.CSVCleanReducer.class);
			job.setReducerClass(MapReduceStage1.CSVCleanReducer.class);

			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(NullWritable.class);

			FileOutputFormat.setCompressOutput(job, true);
			FileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

			FileInputFormat.addInputPath(job, status.getPath());
			String composeOutputPath = output_path+"/"+status.getPath().getName();
			FileOutputFormat.setOutputPath(job, new Path(composeOutputPath));

			job.setInputFormatClass(TextInputFormat.class);
			job.setOutputFormatClass(TextOutputFormat.class);

			success = job.waitForCompletion(true) ? 0 : 1;
			moveSuccess(fs, success, composeOutputPath, stage2_path);
		}
		return success;
	}

	private int run_stage1_reduce_side() throws InterruptedException, IOException, ClassNotFoundException {
		String input_path = parent_input_path+"/"+stage1_path;
		String output_path = parent_output_path+"/"+stage2_path+"/"+reduce_side_path;
		Path inputPath = new Path(input_path);
		Path outputPath = new Path(output_path);
		FileSystem fs = inputPath.getFileSystem(conf);
		int success = 0;

		conf.setBoolean("mapred.output.compress",true);
		conf.set("mapred.output.compression.type", "BLOCK");
		conf.setClass("mapred.map.output.compression.codec", GzipCodec.class, CompressionCodec.class);

		FileStatus[] fileStatus = fs.listStatus(inputPath);
		for(FileStatus status : fileStatus){
			System.out.println(status.getPath().toString());

			Job job = Job.getInstance(conf, "CSV Cleaner Stage 1: "+status.getPath().getName());

			job.setJarByClass(ReduceJoin.class);
			job.setMapperClass(ReduceSide.MapReduceStage1.CSVCleanMapper.class);
			//job.setCombinerClass(MapReduceStage1.CSVCleanReducer.class);
			job.setReducerClass(ReduceSide.MapReduceStage1.CSVCleanReducer.class);

			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(NullWritable.class);

			FileOutputFormat.setCompressOutput(job, true);
			FileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

			FileInputFormat.addInputPath(job, status.getPath());
			String composeOutputPath = output_path+"/"+status.getPath().getName();
			FileOutputFormat.setOutputPath(job, new Path(composeOutputPath));

			job.setInputFormatClass(TextInputFormat.class);
			job.setOutputFormatClass(TextOutputFormat.class);

			success = job.waitForCompletion(true) ? 0 : 1;
			moveSuccess(fs, success, composeOutputPath, stage2_path);
		}
		return success;
	}

	private static void moveSuccess(FileSystem fs, int success, String composeOutputPath, String stage_path) throws IOException {
		FileStatus[] files = fs.listStatus(new Path(composeOutputPath));
		String fileName = "";
		boolean found = false;
		for (FileStatus status : files){
			if(status.getPath().getName().contains("part")){
				found = true;
				fileName = status.getPath().getParent().getName();
			}
		}
		if(success==0 && found){
			String successFile = composeOutputPath+"/_SUCCESS";
			if(fs.exists(new Path(successFile))) {
				fs.delete(new Path(successFile), true);
				fs.rename(new Path(composeOutputPath),new Path(parent_input_path+"/"+stage_path+"/"+fileName));
			} else {
				throw new IOException("No file _SUCCESS found");
			}
			//FileUtil.fullyDelete(new File(successFile));
		}else{
			System.exit(-1);
		}
	}
}
