\def\baselinestretch{1}
\chapter{Big Data's Architecture} \label{cap2}
\def\baselinestretch{1.66}
\paragraph{} From the point of view of the logical abstraction of architecture, both DW and BD have the same components: Data Sources, Extraction, Transformation and Loading processes (ETL), storage, processing and analysis.\\

\section{Sources \& Data Types}\label{sec:2a}
4GL technologies (fourth generation languages) facilitated the development of transactional applications that allowed the automation algorithms on repetitive structured data.
Structured data (SD) is characterized for being well defined, predictable and soundly handled by an elaborated infrastructure.
\\
\\Technological developments, digitization, hyperconnected devices, and social networks, among other enablers, brought unstructured information to the scope of enterprises.
This includes information in digital documents, data coming from autonomous devices (sensors, cameras,
scanners, etc.), and semi-structured data from web sites, social media, emails, etc.
Unstructured data (USD) don't have a predictable and computer recognizable structure, and may be divided into repetitive and non-repetitive data.
Unstructured repetitive data (US-RD) are data that occur in many occasions in time, may have a similar structure, are generally massive, and not always have a value for analysis.
Samples or portions of these data can be utilized.
Because of its repetitive nature, processing algorithms are susceptible of repetition and reutilization.
A typical example of this category is data from sensors, where the objective is the analysis of the signal and for which specific algorithms are defined.
\\
\\Unstructured unrepetitive data (US-URD) have varying data structures, which implies that the algorithms are not reusable (and the task of predicting or describing its structure is already a complex one).
Inmon places elements of textual nature (that require techniques from Natural Language Processing and computational linguistics) inside this category.
From other perspective, besides free-form text, imagery, video and audio also pertain to this category.
Traditional DWs were born with the purpose of integrating structured data coming from transactional sources and to count with historical information that is supported by OLAP-based analysis.
With the upcoming of new data types, some authors propose DW to adapt its architecture and processes, as suggested in Inmon with DW2.0 and Kimball in The Evolving Role of the Enterprise Data Warehouse in the Era of Big Data Analytics.
\\
\\Additionally, one of the things that make big data really big is that it's coming from a greater variety of sources than ever before, including logs, clickstreams, and social media.
Using these sources for analytics means that common structured data is now joined by unstructured data, such as text and human language, and semi-structured data, such as eXtensible Markup Language (XML) or Rich Site Summary (RSS) feeds.
There's also data, which is hard to categorize since it comes from audio, video, and other devices.

\section{Extract-Transform-Load processes (ETL)}\label{sec:2b}
Construction of Data Warehouse requires Extraction, Transformation and Loading processes (ETL).
These must consider several data quality related issues, as for instance duplicated data, possible data inconsistency, high risk in data quality, garbage data, creation of new variables
using transformations, etc.
That raises the need of specific processes to extract enough and necessary information from the sources and implementing processes for cleansing,
transformation, aggregation, classification and estimation tasks.
All these, besides the utilization of different tools for the different ETL processes, can result in fragmented metadata, inconsistent results, rigid models of relational or multidimensional data, and thus lack of flexibility to perform generic analysis and changes.
\\
\\Thus, the need of more flexible ETL processes and improved performance gave birth to proposals such as real time loading instead of batch loading.
Middleware, for instance the engine of flow analysis, was also introduced.
This engine makes a detailed exploration of incoming data (identifies atypical patterns and outliers) before it can be integrated into the cellar.
On the same line is the Operational Data Storage (ODS), that proposes a volatile temporal storage to integrate data from different sources before storing it in the cellar.
%The work presented in, unlike the traditional architectures, creates a ETL subsystem in real time and a periodic ETL process.
%Periodic ETL refers to the periodic importation in batch from the data sources and the ETL in real time.
%Using Change Data Capture (CDC) tools, changes in the data sources are automatically detected and loaded inside the area in real time.
%When the system identifies that certain conditions are met, data are loaded in batch into the cellar.
%The stored part can be then divided in real time and static area.
%Specialized queries for sophisticated analyses are made about storage in real time.
%Static data are equivalent to DW and historical queries are thus handled in the traditional way.
%It's worth to mention that for DW some changes have been observed, including temporal processing areas and individualized processes according to the data access opportunity.

\paragraph{Definition}
In computing, extract, transform, load (ETL) refers to a process in database usage and especially in data warehousing.
The ETL process became a popular concept in the 1970s.
Data extraction is where data is extracted from homogeneous or heterogeneous data sources;
data transformation is where the data is transformed for storing in the proper format or structure for the purposes of querying and analysis;
data loading where the data is loaded into the final target database, more specifically, an operational data store, data mart, or data warehouse.
Since the data extraction takes time, it is common to execute the three phases in parallel.
While the data is being extracted, another transformation process executes while processing the data already received and prepares it for loading while the data loading begins without waiting for the completion of the previous phases.
ETL systems commonly integrate data from multiple applications (systems), typically developed and supported by different vendors or hosted on separate computer hardware.
The separate systems containing the original data are frequently managed and operated by different employees.
For example, a cost accounting system may combine data from payroll, sales, and purchasing.

\subsection{Extract}\label{sec:2b1}
The first part of an ETL process involves extracting the data from the source system(s).
In many cases, this represents the most important aspect of ETL, since extracting data correctly sets the stage for the success of subsequent processes.
Most data-warehousing projects combine data from different source systems.
Each separate system may also use a different data organization and/or format.
Common data-source formats include relational databases, XML, JSON and flat files, but may also include non-relational database structures such as Information Management System (IMS) or other data structures such as Virtual Storage Access Method (VSAM) or Indexed Sequential Access Method (ISAM), or even formats fetched from outside sources by means such as web spidering or screen-scraping.
The streaming of the extracted data source and loading on-the-fly to the destination database is another way of performing ETL when no intermediate data storage is required.
In general, the extraction phase aims to convert the data into a single format appropriate for transformation processing.
An intrinsic part of the extraction involves data validation to confirm whether the data pulled from the sources has the correct/expected values in a given domain (such as a pattern/default or list of values).
If the data fails the validation rules it is rejected entirely or in part.
The rejected data is ideally reported back to the source system for further analysis to identify and to rectify the incorrect records.
In some cases, the extraction process itself may have to do a data-validation rule in order to accept the data and flow to the next phase.

\subsection{Transform}\label{sec:2b2}
In the data transformation stage, a series of rules or functions are applied to the extracted data in order to prepare it for loading into the end target.
Some data does not require any transformation at all; such data is known as "direct move" or "pass through" data.
An important function of transformation is the cleaning of data, which aims to pass only "proper" data to the target.
The challenge when different systems interact is in the relevant systems' interfacing and communicating.
Character sets that may be available in one system may not be so in others.
\begin{itemize}
	\item Selecting only certain columns to load
	\item Translating coded values
	\item Encoding free-form values
	\item Deriving a new calculated value
	\item Sorting or ordering the data based on a list of columns to improve search performance
	\item Joining data from multiple sources (e.g., lookup, merge) and deduplicating the data
	\item Aggregating (for example, rollup, summarizing multiple rows of data)
	\item Generating surrogate-key values
	\item Transposing or pivoting (turning multiple columns into multiple rows or vice versa)
	\item Splitting a column into multiple columns
	\item Disaggregating repeating columns
	\item Looking up and validating the relevant data from tables or referential files
	\item Applying any form of data validation
\end{itemize}

\subsection{Load}\label{sec:2b3}
The load phase loads the data into the end target, which may be a simple delimited flat file or a data warehouse.
Depending on the requirements of the organization, this process varies widely.
Some data warehouses may overwrite existing information with cumulative information;
updating extracted data is frequently done on a daily, weekly, or monthly basis.
Other data warehouses may add new data in a historical form at regular intervals.
As the load phase interacts with a database, the constraints defined in the database schema - as well as in triggers activated upon data load - apply (for example, uniqueness, referential integrity, mandatory fields), which also contribute to the overall data quality performance of the ETL process.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.4]{chapter_2_figure_1.png}\label{fig:cap2fig1}
	\caption{Standard ETL FLow}
\end{figure}
\section{Storing, Processing and Analysis}\label{sec:2c}
Data Warehouse systems have traditionally been supported by predefined multidimensional models (star and snowflake) to support Business Intelligence (BI) and decision making.
These models are generally implemented on relational databases (Relational Online Analytical Processing ROLAP) and managed through Structured Query Language (SQL).
Less frequently, implementations under multidimensional schemes (Multidimensional Online Analytical Processing MOLAP) are also found.
Although the traditional DW manage large amounts of information, its architecture is supported on client-server models that can only be scaled in a vertical way, implying massive technological and economic efforts for both its development and maintenance.
Contrary to this, BD and the new DW generation neither have predefined analytical models, nor rely on client-server architectures and must support the horizontal scaling.
The answer to the new needs is the use of extensive memory, data distribution and processing parallelization, which in one way or another are included in Hadoop, MapReduce, NoSQL databases, storage and processing in memory and technologies complementary to these.

\subsection{In-memory storage and processing}\label{sec:2c1}
In-memory storage and processing improves the performance on data access and analysis.
Several proposals are based on memory-resident tabular or columnar structures, among which we may count SAP HANA, which is a memory-resident database that achieves high analysis performance by means of a structured database oriented to columns and without requiring independent indices.
In-memory databases manage the data in server memory, thus eliminating disk input/output (I/O) and enabling real-time responses from the database.
Instead of using mechanical disk drives, it is possible to store the primary database in silicon-based main memory.
This results in orders of magnitude of improvement in the performance, and allows entirely new applications to be developed.
Furthermore, in-memory databases are now being used for advanced analytics on big data, especially to speed the access to and scoring of analytic models for analysis.
This provides scalability for big data, and speed for discovery analytics.

\subsection{Distribuited Systems: MPP}\label{sec:2c2}
Since current data analyses use complex statistical methods, and analysts need to be able to study enormous datasets by drilling up and down, a big data repository also needs to be deep, and serve as a sophisticated algorithmic runtime engine.
Accordingly, several solutions, ranging from distributed systems and Massive Parallel Processing (MPP) databases for providing high query performance and platform scalability, to non-relational or in-memory databases, have been used for big data.
Non-relational databases, such as Not Only SQL (NoSQL), were developed for storing and managing unstructured, or non-relational, data.
NoSQL databases aim for massive scaling, data model flexibility, and simplified application development and deployment.
Contrary to relational databases, NoSQL databases separate data management and data storage.
Such databases rather focus on the high-performance scalable data storage, and allow data management tasks to be written in the application layer instead of having it written in databases specific languages.