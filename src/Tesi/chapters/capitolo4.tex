\def\baselinestretch{1}
\chapter{Apache Hadoop} \label{cap4}
\def\baselinestretch{1.66}

\paragraph{} To support parallel and distributed processing of large volumes of data, most solutions involve Hadoop and the MapReduce algorithm.
Hadoop is a framework based on distributed processing of large data volumes across multiple clustered systems.
This distribution is based on a file system, Hadoop Distributed File System (HDFS) that provides high performance access to data, is scalable, and offers high availability and tolerance to failures by replication.
To ensure parallel processing, most solutions suggest the use of MapReduce, which with the function Map transforms a dataset in hash pairs to distribute the data segments in different nodes of a cluster, and in this way, parallelize processing.
After processing, the segments are combined into a single result using the reducer function.
The algorithm was initially implemented by Google to solve PageRank processing, but the most referenced implementation is Apache Hadoop.

\paragraph{} Apache Hadoop is an open source distributed software platform for storing and processing data.
Written in Java, it runs on a cluster of industry-standard servers configured with direct-attached storage.
Using Hadoop, you can store petabytes of data reliably on tens of thousands of servers while scaling performance cost-effectively by merely adding inexpensive nodes to the cluster, it provides reliability, scalability, and manageability by providing an implementation for the MapReduce paradigm.
\\
\\Hadoop provides the tools for processing vast amounts of data using the Map/Reduce framework and, additionally, implements the Hadoop Distributed File System (HDFS).
It can be used to process vast amounts of data in-parallel on large clusters in a reliable and fault-tolerant fashion. Consequently, it renders the advantages of the Map/Reduce available to the users.

\paragraph{} The Apache \textit{Hadoop} project develops open-source software for reliable, scalable, distributed computing.\\
The Apache \textit{Hadoop} software library is a framework that allows for the distributed processing of large data sets across clusters of computers using simple programming models.
It is designed to scale up from single servers to thousands of machines, each offering local computation and storage.
Rather than rely on hardware to deliver high-availability, the library itself is designed to detect and handle failures at the application layer, so delivering a highly-available service on top of a cluster of computers, each of which may be prone to failures.\\
The project includes these modules:
\begin{itemize}
	\item Hadoop \textbf{Common}: The common utilities that support the other Hadoop modules.
	\item Hadoop \textbf{Distributed File System} (\textit{HDFS}): A distributed file system that provides high-throughput access to application data.
	\item Hadoop \textbf{YARN}: A framework for job scheduling and cluster resource management.
	\item Hadoop \textbf{MapReduce}: A \textit{YARN}-based system for parallel processing of large data sets.
\end{itemize}
\paragraph{MapReduce} Central to the scalability of Apache Hadoop is the distributed processing framework known as \textbf{MapReduce} (Figure 4.1).
Map/Reduce is a "programming model and an associated implementation for processing and generating large data sets".
It was first developed at Google by Jeffrey Dean and Sanjay Ghemawat.
Their motivation was derived from the multitude of computations that were carried out everyday in Google that involved huge amounts of input data.
\\For instance, finding the most frequent query submitted to Google's search engine on any given day or keeping track of the webpages crawled so far.
But the input to these computations would be so large that it would require distributed processing over hundreds or thousands of machines in order to get results in a reasonable amount of time.
\\And when distributed systems came into picture, a number of problems like carefully distributing the data and partitioning or parallelizing the computation made it difficult for the programmer to concentrate on the actual simple computation.
Dean and Ghemawat saw a need for an abstraction that would help the programmer focus on the computation at hand without having to bother about the complications of a distributed system like fault tolerance, load balancing, data distribution and task parallelization.
And that is exactly what Map\/Reduce was designed to achieve.
%% TODO: HARD CODED Figure Index
\\A simple yet powerful framework which lets the programmer write simple units of work as map and reduce functions.
MapReduce helps programmers solve data-parallel problems for which the data set can be sub-divided into small parts and processed independently.
MapReduce splits the input data-set into multiple chunks, each of which is assigned a map task that can process the data in parallel.
The framework then automatically takes care of partitioning and parallelizing the task on a large cluster of inexpensive commodity machines.
Each \textbf{map} task reads the input as a set of (key, value) pairs and produces a transformed set of (key, value) pairs as the output and shuffles and sorts outputs of the map tasks, sending the intermediate (key, value) pairs to the reduce tasks, which group them into final results.
MapReduce uses \textbf{JobTracker} and \textbf{TaskTracker}.\\
Some of the simple and interesting computations for which Map/Reduce can be used include:
Distributed Grep - finding patterns in a number of files at the same time.
\begin{itemize}
	\item Count of URL access frequency.
	\item Reverse Web-Link Graph - Given a list of htarget, sourcei pair of URLs, finding htarget, list(source)i, i.e., finding all the URLs that link to a given target.
	\item Construction of Inverted Indices from crawled web pages
	\item Distributed Sort
\end{itemize}
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.2]{chapter_4_figure_1.png}\label{fig:cap4fig1}
	\caption{MapReduce Architecture \& Process Flow}
\end{figure}

\paragraph{HDFS} Hadoop platform also includes the \textbf{Hadoop Distributed File System} (HDFS), which is designed for scalability and fault-tolerance.
\\"HDFS is a filesystem designed for storing very large files with streaming data access patterns, running on clusters on commodity hardware."
HDFS was designed keeping in mind the ideas behind Map/Reduce and Hadoop.
\\HDFS stores large files by dividing them into blocks (usually 64 or 128 MB) and replicating the blocks on three or more servers.
These datasets are divided into blocks and stored across a cluster of machines which run the Map/Reduce or Hadoop jobs.
\\This helps the Hadoop framework to partition the work in such a way that data access is local as much as possible.
A very important feature of the HDFS is its "streaming access".
HDFS provides APIs for MapReduce applications to read and write data in parallel.
Capacity and performance can be scaled by adding Data Nodes, and a single NameNode mechanism manages data placement and monitors server availability.
HDFS storage function provides a redundant and reliable distributed file system, which is optimized for large files, where a single file is split into blocks and distributed across cluster nodes.
Additionally, the data is protected among the nodes by a replication mechanism, which ensures availability and reliability despite any node failures.
There are two types of HDFS nodes: the \textbf{Data Nodes} and the \textbf{Name Nodes}.
Data is stored in replicated file blocks across the multiple Data Nodes, and the Name Node acts as a regulator between the client and the Data Node, directing the client to the particular Data Node which contains the requested data.
What this implies is that it is capable of handling datasets of much bigger size than conventional file systems (even petabytes).
\\HDFS works on the idea that the most efficient data processing pattern is a write-once, read-many-times pattern.
Once the data is generated and loaded on to the HDFS, it assumes that each analysis will involve a large proportion, if not all, of the dataset.
So the time to read the whole dataset is more important than the latency in reading the first record.
This has its advantages and disadvantages.
One on hand, it can read bigger chunks of contiguous data locations very fast, but on the other hand, random seek turns out to be a so slow that it is highly advisable to avoid it.
Hence, applications for which low-latency access to data is critical, will not perform well with HDFS.

\paragraph{Apache YARN} Along with the Hadoop file system and the MapReduce programming model, a set of complementary tools that allow distribution, analysis and search have been developed.
Within this group we can mention Yarn (Yet Another Resource Negotiator), which is a tool to plan and monitor tasks and infrastructure resources, allowing different types of MapReduce applications to be run in cluster.
Hadoop Yarn is used to evaluate the performance of the co-location (location of data as close as possible to the processing) under different configurations and workloads.

\paragraph{Apache Flume} Apache Flume is a distributed system for collecting, aggregating, and moving large amounts of data from multiple sources into HDFS or another central data store.
Enterprises typically collect log files on application servers or other systems and archive the log files in order to comply with regulations.
Being able to ingest and analyze that unstructured or semi-structured data in Hadoop can turn this passive resource into a valuable asset.

\paragraph{Apache Sqoop} Apache Sqoop is a tool for transferring data between Hadoop and relational databases.
You can use Sqoop to import data from a MySQL or Oracle database into HDFS, run MapReduce on the data, and then export the data back into an RDBMS.
Sqoop automates these processes, using MapReduce to import and export the data in parallel with fault-tolerance.
Apache Sqoop is an application that habilitated data transfer through relational bases and Hadoop, supporting incremental loads either from a table or a SQL query.

\paragraph{Apache Hive and Apache Pig} Apache Hive and Apache Pig are programming languages that simplify development of applications employing the MapReduce framework.
HiveQL is a dialect of SQL and supports a subset of the syntax.
Although slow, Hive is being actively enhanced by the developer community to enable low-latency queries on Apache HBase and HDFS.
Pig Latin is a procedural programming language that provides high-level abstractions for MapReduce.
You can extend it with User Defined Functions written in Java, Python, and other languages.
Apache \textbf{Hive} manages data storage through HDFS files and bases as Hbase, and allows to do queries on Hadoop through HiveQL language..
\textbf{Pig} is a compiler that manages parallel MapReduce program sequences through PigLatin language.
An \textbf{Hive} competitor is \textbf{Clouera Impala}, that is a query engine for the Massive Parallel Processing (MPP) with Hadoop.
It executes queries with low latency and under the MapReduce framework, without the necessity of transformation or migration of data stored in HDFS or HBase.
Impala will be used later in this paper for a benchmark with Hive.

\paragraph{Apache NiFi} Apache NiFi is an open source tool for automating and managing the flow of data between systems (Databases, Sensors, Data Lakes, Data Platforms).
NiFi is used for data ingestion to pull data into NiFi, from numerous different data sources (sensors, web services, local file system, etc.) and create FlowFiles.

\paragraph{HBase with Hive} ODBC/JDBC Connectors for HBase and Hive are often proprietary components included in distributions for Apache Hadoop software.
They provide connectivity with SQL applications by translating standard SQL queries into HiveQL commands that can be executed upon the data in HDFS or HBase.

\paragraph{} The Table \ref{tab:cap4tab1}, Comparison among Components of Hadoop, gives details of different Hadoop Components which have been used now days.
HBase, Hive, MongoDB, Redis, Cassandra and Drizzle are the different components.
\\Comparison among these components is done on the basis of Concurrency, Durability, Replication Method, Database Model and Consistency Concepts used in the components.
\\
\begin{longtable}{| p{.17\textwidth} | p{.10\textwidth} | p{.12\textwidth} | p{.13\textwidth} | p{.10\textwidth} | p{.13\textwidth} | p{.10\textwidth} |}
	\hline
	\textbf{Name} & \textbf{HBase} & \textbf{Hive} & \textbf{MongoDB} & \textbf{Redis} & \textbf{Cassandra} & \textbf{Drizzle} \\ \hline
	\textbf{Description}
	& Wide-column store based on Apache Hadoop and on concepts of Big Table
	& Data Warehouse Software for Querying and Managing Large Distributed Datasets, built on Hadoop
	& One of the most popular Document Stores
	& In-memory Database with configurable options performance vs. persistency
	& Wide-column store based on BigTable and DynamoDB
	& MySQL fork with a pluggable micro-kernel and with an emphasis of perfomance over compatiblity
	\\ \hline
	\textbf{Implementation language}
	& Java
	& Java
	& C++
	& C
	& Java
	& C++
	\\ \hline
	\textbf{Database Model}
	& Wide Column Store
	& Relational DBMS
	& Document Store
	& Key - Value Store
	& Wide Column Store
	& Relational DBMS
	\\ \hline
	\textbf{Consistency Concepts}
	& Immediate Consistency
	& Eventual Consistency
	& Eventual Consistency, Immediate Consistency
	& -
	& Eventual Consistency, Immediate Consistency
	& -
	\\ \hline
	\textbf{Concurrency}
	& Yes
	& Yes
	& Yes
	& Yes
	& Yes
	& Yes
	\\ \hline
	\textbf{Durability}
	& Yes
	& Yes
	& Yes
	& Yes
	& Yes
	& Yes
	\\ \hline
	\textbf{Replicaiton Method}
	& Selected Replication factor
	& Selected Replication factor
	& Master - Slave Replication
	& Master - Slave Replication
	& Selected Replication factor
	& Master - Master Replication, Master - Slave Replication
	\\ \hline
	\caption{Comparison among Components of Hadoop}
	\label{tab:cap4tab1}
\end{longtable}