\def\baselinestretch{1}
\chapter{Case Study: Hadoop Architecture} \label{cap6}
\def\baselinestretch{1.66}

\paragraph{} Hadoop introduced a new way to simplify the analysis of large data sets, and in a very short time reshaped the big data market.
In fact, today Hadoop is often synonymous with the term big data.
Since Hadoop is an open source project, a number of vendors have developed their own distributions, adding new functionality or improving the code base.
A standard open source Hadoop distribution (Apache Hadoop) includes:
\begin{itemize}
	\item The Hadoop MapReduce framework for running computations in parallel
	\item The Hadoop Distributed File System (HDFS)
	\item Hadoop Common, a set of libraries and utilities used by other Hadoop modules
\end{itemize}
\paragraph{} This is only a basic set of Hadoop components;
there are other solutions -- such as Apache Hive, Apache Pig, and Apache Zookeeper, etc.
-- that are widely used to solve specific tasks, speed up computations, optimize routine tasks, etc.
Vendor distributions are, of course, designed to overcome issues with the open source edition and provide additional value to customers, with a focus on things such as:
\begin{itemize}
	\item Reliability.
	The vendors react faster when bugs are detected.
	They promptly deliver fixes and patches, which makes their solutions more stable.
	\item Support.
	A variety of companies provide technical assistance, which makes it possible to adopt the platforms for mission-critical and enterprise-grade tasks.
	\item Completeness.
	Very often Hadoop distributions are supplemented with other tools to address specific tasks.
\end{itemize}
\paragraph{} In addition, vendors participate in improving the standard Hadoop distribution by giving back updated code to the open source repository, fostering the growth of the overall community.
There are differents Hadoop distribution, the most important described below:
\begin{itemize}
	\item Cloudera CDH
	\item Hortonworks HDP Sandbox
	\item MapR M3
\end{itemize}

\section{Hadoop Cluster} \label{cap6sec1}
\paragraph{} A Hadoop cluster consists of the following main components, all of which are implemented as JVM daemons:
\begin{itemize}
	\item JobTracker\\
	Master node controlling the distribution of a Hadoop (Map/Reduce) Job across free nodes on the cluster.
	It is responsible for scheduling the jobs on the various \textit{TaskTracker} nodes.
	In case of a node-failure, the \textit{JobTracker} starts the work scheduled on the failed node on another free node.
	The simplicity of Map/Reduce tasks ensures that such restarts can be achieved easily.
	\item NameNode
	Node controlling the HDFS. It is responsible for serving any component that
	needs access to files on the HDFS. It is also responsible for ensuring faulttolerance
	on HDFS. Usually, fault-tolerance is achieved by replicating the files
	over 3 different nodes with one of the nodes being an off-rack node.
	\item TaskTracker
	Node actually running the Hadoop Job. It requests work from the \textit{JobTracker}
	and reports back on updates to the work allocated to it. The \textit{TaskTracker} daemon
	does not run the job on its own, but forks a separate daemon for each task
	instance.
	This ensure that if the user code is malicious it does not bring down
	the \textit{TaskTracker}
	\item DataNode
	This node is part of the HDFS and holds the files that are put on the HDFS.
	Usually these nodes also work as \textit{TaskTracker}s. The \textit{JobTracker} tries to allocate
	work to nodes such files accesses are local, as much as possible.
\end{itemize}

\section{Hadoop Jobs} \label{cap6sec2}
The figure 6.1 shows Map/Reduce workflow in a user job submitted to Hadoop.\\
To run a Map/Reduce or Hadoop job on a Hadoop cluster, the client program must create a JobConf configuration file.
This configuration file:
\begin{itemize}
	\item Identifies classes implementing Mapper and Reducer interfaces. \dots{}
		\begin{itemize}[label=$\star$]
			\item \textit{JobConf.setMapperClass(), setReducerClass()}
		\end{itemize}
	\item Specifies inputs, outputs \dots{}
		\begin{itemize}[label=$\star$]
			\item \textit{FileInputFormat.addInputPath(conf)}
			\item \textit{FileOutputFormat.setOutputPath(conf)}
		\end{itemize}
	\item Optionally, other options too: \dots{}
		\begin{itemize}[label=$\star$]
			\item \textit{JobConf.setNumReduceTasks()}
			\item \textit{JobConf.setOutputFormat()}
		\end{itemize}
\end{itemize}
\begin{wrapfigure}{R}{0.45\textwidth}
	\centering
	\includegraphics[width=0.4\textwidth]{chapter_6_figure_1.png}\label{fig:cap6fig1}
	\caption{Hadoop Job}
\end{wrapfigure}
\paragraph{} The \textit{JobTracker} inserts the program (supplied in the form of a .jar file) and the \textit{JobConf} file in a shared location.
\textit{TaskTrackers} running on slave nodes periodically query the \textit{JobTracker} for work and retrieve job-specific jar and configuration files.
As mentioned earlier, they then launch tasks in separate instances of Java Virtual Machine.

\subsection{Map and Reduce Functions} \label{cap6sec2sub1}
\paragraph{} The \textit{Map} and \textit{Reduce} classes in Hadoop extend \textit{MapReduceBase} class and the \textbf{map} and \textbf{reduce} functions have the signature as shown in figure 6.2.
Hadoop has its own serialization format.
Central to this serialization format is the \textit{Writable} interface, which is a part of the signatures shown in figure 6.2.
\textit{WritableComparable} is a sub-interface of \textit{Writable} that requires the implementation of a \textit{Comparator} function.
\textit{OutputCollector} is a generic class used purely for emitting key/value pairs.
\textit{Reporter} is for updating counters and statuses.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{chapter_6_figure_2.png}\label{fig:cap6fig2}
	\caption{Signature of Map and Reduce methods}
\end{figure}

\subsection{Partition and Grouping}\label{cap6sec2sub2}
\paragraph{} The Hadoop framework takes the output from the \textit{Mapper} and does the following:
\begin{enumerate}
	\item Partitions the output
	\item Sorts the individual partitions
	\item Sends relevant partitions to Reducers
	\item Merges the partitions received from different Mappers
	\item Groups the tuples in the partition based on key and calls the reduce function
\end{enumerate}
\paragraph{} The figure 6.3 shows the steps just described in the context of the entire Hadoop (Map/Reduce) job.
A 'Partitioner' class can be defined by implementing the \textit{Partitioner} interface of the \textit{org.apache.hadoop.mapred} package.
\\And this class needs to be specified as part of the Job Configuration by using \textit{JobConf.setPartitionerClass()} function.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.4]{chapter_6_figure_3.png}\label{fig:cap6fig3}
	\caption{Shuffle and Sort in Map/Reduce}
\end{figure}
\paragraph{} To override the grouping done in the \textit{Reducer} one needs to write a class that extends
\\\textit{org.apache.hadoop.io.WritableComparator} class.\\
This class must have functions to compare key values, but the behaviour of the comparison is completely dependent on
the programmer.
To make sure the \textit{Reducer} uses this class for comparison, it needs to be specified as part of the Job Configuration by using
\textit{JobConf.setOutputValueGroupingComparator()} function.

\subsection{Hadoop Counter}\label{cap6sec2sub3}
\paragraph{} When a client program initiates a Hadoop job, the Hadoop framework keeps track of a lot of metrics and counters as part of its logs.
These are usually system metrics like time taken, amount of data read, etc.
Apart from these, Hadoop also allows users to specify their own user-defined counters which are then incremented as desired in the \textit{mapper} and the \textit{reducer}.
\\There are two ways of defining a counter:
\begin{itemize}
	\item Java \textit{enum}:\\
	Users can define any number of Java enums with any number of fields in them. An example is:\\
	\textbf{\tab enum NumType\{\\
	\tab\tab EVEN,\\
	\tab\tab ODD\\
	\tab\}}\\
	These can be incremented in the \textit{mapper} or the \textit{reducer} as:\\
	\textbf{Reporter.incrementCounter(NumType.EVEN, 1)}
	\item Dynamic Counters\\
	Apart from \textit{enums}, users can also create dynamic counters.
	The user will have to make sure he/she uses the same name every where.
	An example is:\\
	\textbf{Reporter.incrementCouner("NumType", "Even", 1)}
\end{itemize}

\subsection{Hadoop Data Types}\label{cap6sec2sub4}
\paragraph{} Hadoop uses its own serialization format called \textit{Writables}.
It is fast and compact.
For ease of use, Hadoop comes with built-in wrappers for most of Java primitives.
The only one that is used in this project \textit{Text}, which is a wrapper around the Java \textit{String} object.

\subsection{Compression in Hadoop}\label{cap6sec2sub5}
\paragraph{} Compression can be easily implemented in Hadoop.\\
It has built-in libraries or \textit{codecs} that implement most of the well-known compression algorithms.\\
Shown below are two of the most famous ones used.\\
The interesting thing to note is that some of the codecs allow you to split and some dont.
\begin{table}[h!]
	\centering
	\begin{tabular}{ l l }
		\hline
		\textbf{Compression Format} & \textbf{Hadoop CompressionCodec} \\
		\hline
		gzip & org.apache.hadoop.io.compress.GzipCodec \\
		\hline
		bzip2 & org.apache.hadoop.io.compress.BZip2Codec \\
		\hline
	\end{tabular}
	\caption{Hadoop Compression Codecs}
	\label{tab:cap6tab1}
\end{table}

\section{MapReduce Jobs} \label{cap6sec3}
\paragraph{} To create a Map/Reduce job, a programmer specifies a \textbf{map} function and a \textbf{reduce} function.
This abstraction is inspired by the 'map' and 'reduce' primitives present in Lisp and many other functional languages.
The Map/Reduce framework runs multiple instance of these functions in parallel.
The \textbf{map} function processes a key/value pair to generate another key/value pair.
A number of such \textbf{map} functions running in parallel on the data that is partitioned across the cluster, produce a set of intermediate key/value pairs.
The \textbf{reduce} function then merges all intermediate values that are associated with the same intermediate key (see figure 6.4).
\begin{center}
	\textbf{map (k1, v1) => k2,v2}\\
	\textbf{reduce (k2, list(v2)) => v3}
\end{center}
\paragraph{} Programs written in this functional style are automatically parallelized by the Map/Reduce framework and executed on a large cluster of commodity machines.
As mentioned earlier, the run-time system takes care of the details of data distribution, scheduling the various \textbf{map} and \textbf{reduce} functions to run in parallel across the set of machines, handling machine failures, and managing the required inter-machine communication.
This allows programmers without any prior experience with parallel and distributed systems to easily utilize the resources of a large distributed system.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{chapter_6_figure_4.png}\label{fig:cap6fig4}
	\caption{Map Reduce Execution Overview}
\end{figure}

\subsection{MapReduce Breakdown}\label{cap6sec3sub1}
\paragraph{} Figure 6.4 gives an overview of the Map/Reduce execution model.
Before explaining the steps involved in a Map/Reduce job, lets clarify the terminology that will be used from this point on in this thesis (unless specifically specified otherwise).
\begin{itemize}
	\item Machine - an actual physical computer, which is part of a distributed cluster
	\item Task or Worker - a process running on a machine
	\item Node - This can be thought of as a process handler.
	This will become more clear when we discuss Hadoop where a Node is basically a \textit{Java Daemon}.
	Nodes run on the machines that are part of the cluster.
	Ideally, one machine will correspond to one node.
\end{itemize}
An entire "Map/Reduce Job" can be broken down into the following steps (reproduced here from with a few modifications):
\begin{enumerate}
	\item The Map/Reduce framework first splits the input data files into \textit{M} pieces of fixed size - this typically being 16 megabytes to 64 megabytes (MB) per piece (controllable by the user via an optional parameter).
	These \textit{M} pieces are then passed on to the participating machines in the cluster.
	Usually there are 3 copies (user controllable) of each piece for fault tolerance purposes.
	It then starts up many copies of the user program on the nodes in the cluster.
	\item One of the nodes in the cluster is special - the master.
	The rest are workers that are assigned work by the master.
	There are \textit{M} map tasks and \textit{R} reduce tasks to assign.
	\textit{R} is either decided by the configuration specified with the user-program, or by the cluster wide default configuration.
	The master picks idle workers and	assigns each one a map task.
	Once the map tasks have generated the intermediate output, the master then assigns reduce tasks to idle workers.
	Note that all map tasks have to finish before any reduce task can begin.
	This is because the reduce tasks take output from any and every map task that may generate an output that
	it will need to consolidate.
	\item A worker who is assigned a map task reads the contents of the corresponding input split.
	It parses key/value pairs out of the input data and passes each pair to an instance of the user defined \textbf{map} function.
	The intermediate key/value pairs produced by the \textbf{map} functions are buffered in memory at the respective machines that are executing them.
	\item The buffered pairs are periodically written to local disk and partitioned into \textit{R} regions by the \textit{partitioning} function.
	The framework provides a default \textit{partitioning} function but the user is allowed to override this function for allowing 	custom partitioning.
	The locations of these buffered pairs on the local disk are passed back to the master.
	The master then forwards these locations to the reduce workers.
	\item When a reduce worker is notified by the master about these locations, it uses remote procedure calls to read the buffered data from the local disks of the map workers.
	When a reduce worker has read all intermediate data, it sorts it by the intermediate keys (\textbf{k2} in the definition given earlier for the \textbf{reduce} function) so that all occurrences of the same key are grouped together.
	The sorting is needed because typically many different keys map to the same reduce task.
	If the amount of intermediate data is too large to fit in memory, an external sort is used.
	Once again, the user is allowed to override the default sorting and grouping behaviours of the framework.
	\item Next, the reduce worker iterates over the sorted intermediate data and for each unique intermediate key encountered, it passes the key and the corresponding set of intermediate values to the users \textbf{reduce} function.
	The output of the \textbf{reduce} function is appended to a final output file for this reduce partition.
	\item When all map tasks and reduce tasks have been completed, the master wakes up the user program.
	At this point, the Map/Reduce call in the user program returns back to the user code.
\end{enumerate}

\section{Join Algorithms} \label{cap6sec4}
\paragraph{Remarks} Before moving any further, let us define what these are specifically in context of this project:
\begin{itemize}
	\item Two-way Joins - Given two dataset $P$ and $Q$, a two-way join is defined as a
	combination of tuples $p \in P$ and $q \in Q$, such that $p.a = q.b$.
	$a$ and $b$ are values from columns in $P$ and $Q$ respectively on which the join is to be done.
	Please note that this is specifically an 'equi-join' in database terminology.
	This can be	represented as: $$P\bowtie_{a=b} Q$$
	\item Multi-way Joins - Given $n$ datasets $P_1,P_2,\dots,P_n$, a multi-way join is defined as a combination of tuples $p_1 \in P_1, p_2 \in P_2,\dots,p_n \in P_n$, such that $p_1.a_1 = p_2.a_2 =\dots= p_n.a_n$. $a_1,a_2,\dots,a_n$ are values from columns in $P_1,P_2,\dots,P_n$ respectively on which the join is to be done.
	Notice once again that this is specifically an 'equi-join'.
	This can be represented as: $$P_1\bowtie_{a_1=a_2} P_2\bowtie_{a_2=a_3}\dots\bowtie_{a_{n-1}=a_n} P_n$$
\end{itemize}
\subsection{Join Algorithms in standard database context}\label{cap6sec4sub1}
\paragraph{} Before jumping on to join algorithms using Map/Reduce (or Hadoop) it might be a good idea to review the currently exiting join algorithms in the standard database context.
There are three of them that are very popular.
These are
\begin{enumerate}
	\item Sort-Merge Join
	\item Hash Join
\end{enumerate}
\paragraph{Sort-Merge Join} Given two datasets $P$ and $Q$, the \textit{Sort-Merge Join} algorithm sorts both datasets on the join attribute and then looks for qualifying tuples $p \in P$ and $q \in Q$ by essentially
merging the two datasets.
The sorting step groups all tuples with the same value in the join column together and thus makes it easy to identify partitions or groups of tuples with the same value in the join column.
This partitioning is exploited by comparing the $P$ tuples in a partition with only the $Q$ tuples in the same partition (rather than with all tuples in $Q$), thereby avoiding enumeration or the cross-product of $P$ and $Q$\@.
This partition-based approach works only for equality join conditions.\\
\\$p \in P;q \in Q;gq \in Q$\\
\textbf{while} more tuples in inputs \textbf{do}\\
\\\tab	\textbf{while} $p.a < gq.b$ \textbf{do}\\
\tab\tab		advance $p$\\
\tab	\textbf{end while}\\
\tab	\textbf{while} $p.a > gq.b$ \textbf{do}\\
\tab\tab		advance $gq$ {a group might begin here}\\
\tab	\textbf{end while}\\
\tab	\textbf{while} $p.a == gq.b$ \textbf{do}\\
\tab\tab		$q = gq$ {mark group beginning}\\
\tab	\textbf{while} $p.a == q.b$ \textbf{do}\\
\tab\tab		Add $\langle p,q\rangle$ to the result\\
\tab\tab		Advance $q$\\
\tab	\textbf{end while}\\
\tab\tab		Advance $p$ {move forward}\\
\tab	\textbf{end while}\\
\tab	$gq = q$ {candidate to begin next group}\\
\textbf{end while}\\
\\It should be apparent that this algorithm will give good performances if the input datasets are already sorted.
\\The \textit{Reduce-Side Join} algorithm is in many ways a distributed version of \textit{Sort-Merge Join}. In \textit{Reduce-Side Join} algorithm, each sorted partition is sent to a \textbf{reduce} function for merging.

\paragraph{Hash Join} The Hash Join algorithm consists of a 'build' phase and a 'probe' phase.
In its simplest variant, the smaller dataset is loaded into an in-memory hash table in the build phase.
\\In the 'probe' phase, the larger dataset is scanned and joined with the relevant tuple(s) by looking into the hash table.
The algorithm like the Sort-Merge Join algorithm, also works only for equi-joins.
\\Consider two datasets $P$ and $Q$.
The algorithm for a simple hash join will look like this:\\\\
\textbf{for all} $p \in P$ \textbf{do}\\
\tab Load $p$ into in memory hash table $H$\\
\textbf{end for}\\
\textbf{for all} $q \in Q$ \textbf{do}\\
\tab \textbf{if} $H$ contains $p$ matching with $q$ \textbf{then}\\
\tab\tab add $\langle p,q\rangle$ to the result\\
\tab\textbf{end if}\\
\textbf{end for}\\
\\This algorithm usually is faster than the \textit{Sort-Merge Join}, but puts considerable load on memory for storing the hash-table.
\\The \textit{Broadcast Join} algorithm is a distributed variant of the above mentioned \textit{Hash Join} algorithm.
The smaller dataset is sent to every node in the distributed cluster.
It is then loaded into an in-memory hash table.
Each \textbf{map} function streams through its allocated chunk of input dataset and probes this hash table
for matching tuples.

\subsection{Two-Way Joins}\label{cap6sec4sub2}
\subsubsection{Reduce-Side Join}\label{cap6sec4sub2sub1}
\paragraph{} In this algorithm, as the name suggests, the actual join happens on the Reduce side of
the framework.
The 'map' phase only pre-processes the tuples of the two datasets to
organize them in terms of the join key.
\paragraph{Map Phase}
%% TODO: Appendix A
The \textbf{map} function reads one tuple at a time from both the datasets via a stream from HDFS. The values from the column on which the join is being done are fetched as keys to the \textbf{map} function and the rest of the tuple is fetched as the value associated with that key.
It identifies the tuples' parent dataset and tags them. A custom class
called TextPair (See Appendix A) has been defined that can hold two \textit{Text} values. The \textbf{map} function uses this class to tag both the key and the value. The reason for tagging both of them will become clear in a while. Figure 6.6 is the code for the \textbf{map} function.\\
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.3]{chapter_6_figure_5.png}\label{fig:cap6fig5}
	\caption{Reduce Side Join - Two Way}
\end{figure}
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{chapter_6_figure_6.png}\label{fig:cap6fig6}
	\caption{\textit{Reduce Side Join} - \textbf{map} function}
\end{figure}

\paragraph{Partitioning and Grouping Phase}
The partitioner partitions the tuples among the reducers based on the join key such that all tuples from both datasets having the same key go to the same reducer.
The default partitioner had to be specifically overridden to make sure that the partitioning was done only on the Key value, ignoring the Tag value.
The Tag values are only to allow the reducer to identify a tuple's parent dataset.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.45]{chapter_6_figure_7.png}\label{fig:cap6fig7}
	\caption{\textit{Reduce Side Join} - Partitioner function}
\end{figure}
\\But this is not sufficient.
Even though this will ensure that all tuples with the same key go to the same Reducer, there still exists a problem.
The \textbf{reduce} function is called once for a key and the list of values associated with it.
This list of values is generated by grouping together all the tuples associated with the same key
\\We are sending a composite TextPair key and hence the Reducer will consider (key, tag) as a key. This means, for eg., two different \textbf{reduce} functions will be called for [Key1, Tag1] and [Key1, Tag2].
To overcome this, we will need to override the default grouping function as well.
This function is essentially the same as the partitioner and  makes sure the reduce groups are formed taking into consideration only the Key part and ignoring the Tag part.
See figure 6.8 to understand better.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.4]{chapter_6_figure_8.png}\label{fig:cap6fig8}
	\caption{Custom Partitioning and Grouping}
\end{figure}
\newpage
\paragraph{Reduce Phase}
The framework sorts the keys (in this case, the composite TextPair key) and passes them on with the corresponding values to the Reducer.
Since the sorting is done on the composite key (primary sort on Key and secondary sort on Tag), tuples from one table will all come before the other table.
The Reducer then calls the \textbf{reduce} function for each key group.
The reduce function buffers the tuples of the first dataset.
This is required since once the tuples are read from the HDFS stream, we lose access to these values unless we reinitialize the stream.
But we need these tuples in order to join them with the tuples from the second dataset.
Tuples of the second dataset are simply read directly from the HDFS stream, one at time and joined with all the tuples from the buffer (all the values with one \textbf{reduce} function are for the
same key).
It was mentioned earlier that both key and values need to be tagged.
This is because the tag attached with the key is used to do a secondary sort to ensure all tuples from one table are processed before the other.
Once the \textbf{reduce} function is called, it will only get the first (key, tag) pair as its key (owing to the custom grouping function written which ignores the tags).
Hence the values also need to be tagged for the \textbf{reduce} function to identify their parent datasets.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{chapter_6_figure_9.png}\label{fig:cap6fig9}
	\caption{\textit{Reduce Side Join} - \textbf{reduce} function}
\end{figure}
\newpage
Note that the this algorithm makes no assumption about the number of times a key repeats in either of the datasets and can handle duplicate keys.

\subsubsection{Map-Side Join}\label{cap6sec4sub2sub2}
The Reduce-Side join seems like the natural way to join datasets using Map/Reduce.
It uses the framework's built-in capability to sort the intermediate key-value pairs before they reach the Reducer.
But this sorting often is a very time consuming step.
Hadoop offers another way of joining datasets before they reach the Mapper.
This functionality is present out of the box and is arguably is the fastest way to join two datasets using Map/Reduce, but places severe constraints (see table \ref{tab:cap6tab2}) on the datasets that can be used for the join.\\
\begin{longtable}{ p{.28\textwidth} p{.65\textwidth} }
	\hline
	\textbf{Limitation} & \textbf{Why} \\
	\hline
	All datasets must be sorted using the same comparator. & The sort ordering of the data in each dataset must be identical for datasets to be joined.\\
	All datasets must be partitioned using the same partitioner.
	& Agiven key has to be in the same partition in each dataset so that all partitions that can hold a key are joined together.\\
	The number of partitions in the datasets must be identical.
	& A given key has to be in the same partition in each dataset so that all partitions that can hold a key are joined together.\\
	\caption{Limitation of Map-Side Join}
	\label{tab:cap6tab2}
\end{longtable}
These constraints are quite strict but are all satisfied by any output dataset of a Hadoop job.
Hence as a pre-processing step, we simply pass both the dataset through a basic Hadoop job.
This job uses an \textit{IdentityMapper} and an \textit{IdentityReducer} which do no processing on the data but simply pass it through the framework which partitions, groups and sorts it.
The output is compliant with all the constraints mentioned above.\\
Although the individual map tasks in a join lose much of the advantage of data locality, the overall job gains due to the potential for the elimination of the reduce phase and/or the great reduction in the amount of data required for the reduce.
This algorithm also supports duplicate keys in all datasets.
More can be found out about this kind of join in.

\subsubsection{Broadcast Join}\label{cap6sec4sub2sub3}
If one of the datasets is very small, such that it can fit in memory, then there is an optimization that can be exploited to avoid the data transfer overhead involved in transferring values from \textit{Mappers} to \textit{Reducers}.
This kind of scenario is often seen in real-world applications.
For instance, a small users database may need to be joined with a large log.
This small dataset can be simply replicated on all the machines.
This can be achieved by simply using \textit{-files} or \textit{-archive} directive to send the file to each machine while invoking the Hadoop job.\\
Broadcast join is a Map-only algorithm.

\paragraph{Map Phase}
The \textit{Mapper} loads the small dataset into memory and calls the \textbf{map} function for each tuple from the bigger dataset.
For each (key, value), the \textbf{map} function probes the inmemory dataset and finds matches.
This process can be further optimized by loading the small dataset into a Hashtable.
It then writes out the joined tuples.
The below shown \textit{configure} function is part of the \textit{Mapper} class and is called once for every \textit{Mapper}.
The below code reads the 'broadcasted' file and loads the same into an inmemory
hash table.\\
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.8]{chapter_6_figure_10.png}\label{fig:cap6fig10}
	\caption{Loading the small dataset into a Hash table}
\end{figure}
\newpage
The next piece of code is the actual \textbf{map} function that receives the records from the HDFS and probes the HashTable containing the tuples from the broadcasted file.
\\Notice that it takes care of duplicate keys as well.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{chapter_6_figure_11.png}\label{fig:cap6fig11}
	\caption{Broadcast Join Map Function}
\end{figure}
\newpage
\textit{Broadcast Join} benefits from the fact that it uses a small 'local' storage on the individual nodes instead of the HDFS.
This makes it possible to load the entire dataset into an in-memory hash table, access to which is very fast.
But on the down side, it will run into problems when both the datasets are large and neither of them can be stored locally on the individual nodes.

\subsection{Multi-Way Joins}\label{cap6sec4sub3}
\subsubsection{Map-Side Join}\label{cap6sec4sub3sub1}
Map-Side joins are the same when it comes to Multi-Way joins.
They can handle as many datasets as long as they conform to the constraints (Table \ref{tab:cap6tab2}) mentioned earlier.

\subsubsection{Reduce-Side One-Shot Join}\label{cap6sec4sub3sub2}
This is basically an extension of the Reduce-Side join explained earlier.
\\A list of tables is passed as part of the job configuration (passes as \textit{tables.tags}) to enable the \textit{Mapper} and \textit{Reducer} to know how many tables to expect and what tags are associated with which table.
For instance, to join $n$ datasets, $T_1,T_2,T_3,\dots,T_n$, this algorithm in simple relational algebra can be represented as:
$$T_1 \bowtie T2 \bowtie \dots \bowtie T_{n-1} \bowtie T_n$$

\paragraph{Map Phase}
The \textit{Mapper} reads \textit{tables.tags} from the job configuration and calls the \textbf{map} function.
The \textbf{map} function then proceeds to tag the tuples based on the dataset they originate from.
This is similar to the map phase for two-way join.

\paragraph{Partitioning and Grouping Phase}
The \textit{Partitioner} and the \textit{} function are the exact same as explained for two-way join.
They partition and group based on just the key, ignoring the tag.

\paragraph{Reduce Phase}
The reduce phase is slightly more involved than the two sided join.
The \textit{Reducer} as usual, gets the tuples sorted on the (key, tag) composite key.
All tuples having the same value for the join key, will be received by the same \textit{Reducer} and only one \textbf{reduce} function is called for one key value.
Based on the number of tables passed as part of the job configuration, the \textit{Reducer} dynamically creates buffers to hold all but the last datasets.
The last dataset is simply streamed from the file system.
As a necessary evil, required to avoid running out of memory, the buffers are spilled to disk if they exceed a certain pre-defined threshold.
This quite obviously will contribute to I/O and runtime.
\\Once the tuples for a particular key are divided as per their parent datasets, it is now a cartesian product of these tuples.
Subsequently, the joined tuples are written to the output.
\paragraph{Advantages and Drawbacks}
\begin{itemize}
	\item Advantages
	\begin{enumerate}
		\item Joining in one go means no setting up of multiple jobs.
		\item No intermediate results involved which could lead to substantial space savings.
	\end{enumerate}
	\item Disadvantages
	\begin{enumerate}
		\item Buffering tuples can easily run in to memory problems, especially if the data is skewed.
		\item If used for more than 5 datasets, its highly likely that the buffer will overflow.
	\end{enumerate}
\end{itemize}

\subsubsection{Reduce-Side Cascade Join}\label{cap6sec4sub3sub3}
This is a slightly different implementation of the \textit{Reduce-Side Join} for Multi-Way joins.
\\Instead joining all the datasets in one go, they are joined two at a time.
In other words, it is an iterative implementation of the two-way \textit{Reduce-Side Join}.
The calling program is responsible for creating multiple jobs for joining the datasets, two at a time. Considering $n$ tables, $T_1,T_2,T_3,\dots,T_n$, $T_1$ is joined with $T_2$ as part of one job.
The result of this join is joined with $T_3$ and so on.
Expressing the same in relational algebra:
$$(\dots(((T_1 \bowtie T_2) \bowtie T_3) \bowtie T_4)\dots\bowtie T_{n-1}) \bowtie T_n$$
\paragraph{Advantages and Drawbacks}
\begin{itemize}
	\item Advantages
	\begin{enumerate}
		\item Data involved in one Map/Reduce job is lesser than the algorithm One-Shot Join.
		Hence lesser load on the buffers and better I/O.
		\item Datasets of any size can be joined provided there is space available on the HDFS.
		\item Any number of datasets can be joined given enough space on the HDFS.
	\end{enumerate}
	\item Disadvantages
	\begin{enumerate}
		\item Intermediate results can take up a lot of space.
		But this can be optimized to some extent as explained in the next section.
		\item Setting up the multiple jobs on the cluster incurs a non-trivial overhead.
	\end{enumerate}
\end{itemize}

\paragraph{Reduce-Side Cascade Join - Optimization}
Simply joining datasets two at a time is very inefficient.
The intermediate results are usually quite large and will take up a lot of space.
Even if these are removed once the whole join is complete, they could put a lot of strain on the system while the cascading joins are taking place.
Perhaps there is a way of reducing the size of these intermediate results.
There are two ways this can be achieved:
\begin{itemize}
	\item Compressing the intermediate results
	\item Join the datasets in increasing order of the output cardinality of their joins, i.e., join the datasets that produce the least number of joined tuples first, then join this result with the next dataset which will produce the next lowest output cardinality.
\end{itemize}

\paragraph{Optimization using compression}
Compressing the results will not only save space on the HDFS, but in case the subsequent join's \textit{Map} task needs to fetch a non-local block for processing, it will also result in lower number of bytes transferred over the network.

\paragraph{Optimization using Output Cardinality}
Deciding the order of joining datasets using the output cardinality is more involved.\\
Lets consider two sample datasets:\\
\begin{table}[h!]
	\centering
	\begin{tabular}{|l|l|}
		\hline
		Key & Value \\ \hline
		1 & ABC \\ \hline
		2 & DEF \\ \hline
		2 & GHI \\ \hline
		3 & JKL \\ \hline
	\end{tabular}
	\begin{tabular}{|l|l|}
		\hline
		Key & Value \\ \hline
		1 & LMN \\ \hline
		2 & PQR \\ \hline
		3 & STU \\ \hline
		3 & XYZ \\ \hline
	\end{tabular}
	\caption{Sample Datasets}
	\label{tab:cap6tab3}
\end{table}
\\The output of joining these two datasets will be:
\begin{table}[h!]
	\centering
	\begin{tabular}{|l|l|l|}
		\hline
		Key & Table 1 & Table 2 \\ \hline
		1 & ABC & LMN \\ \hline
		2 & DEF & PQR \\ \hline
		2 & GHI & PQR \\ \hline
		3 & JKL & STU \\ \hline
		3 & JKL & XYZ \\ \hline
	\end{tabular}
	\caption{Joined result of the sample datasets}
	\label{tab:cap6tab4}
\end{table}
\newpage
The thing to notice is that the output will have:\\
$$T_1(K_1) * T_2(K_1)+T_1(K_2) * T_2(K_2) \dots T_1(K_n) * T_2(K_n)$$
$$=\sum\limits_{i=1}^n T_1(K_i)*T_2(K_i)$$
\\number of rows. where $T_m(K_n)$ represents the number of tuples in table $T_m$ having the key $K_n$.
In our case, it will be:\\
$$1*1 + 2*1 + 1*2 =5$$
Hence, if we find out the number of keys in each dataset and the corresponding number of tuples associated with each key in those datasets, we can find the number of output tuples in the join, or in other words, the output cardinality of the joined dataset.
This can be very easily done as part of a pre-processing step.
This pre-processing is done using counters that were mentioned above.
To accomplish this, output cardinalities are found pair-wise for all datasets at pre-processing time and stored in files.
When the join is to be executed, the datasets with the lowest output cardinality are chosen and joined first.
This intermediate result this then joined with the dataset that has the lowest join output cardinality with either of the two tables joined previously.
In the next round, the dataset with the lowest join output cardinality with any of the three datasets joined so far is chosen to join with the intermediate result and so on.
The next chapter shows the results of experimental evaluation and compares and contrasts the gains that such optimization leads to.