package MapSide;

import MainJob.MYCOUNTER;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class MapReduceStage2 {
	private static String regioni_txt = "regioni_txt";
	public static class MapperMapSideJoinDCacheTextFile extends
			Mapper<LongWritable, Text, Text, Text> {

		private static HashMap<String, String> RegionMap = new HashMap<String, String>();
		private BufferedReader brReader;
		private String strRegionName = "";
		private Text txtMapOutputKey = new Text("");
		private Text txtMapOutputValue = new Text("");

		@Override
		public void run(Context context) throws IOException, InterruptedException {
			super.run(context);
		}

		@Override
		protected void setup(Context context) throws IOException,
				InterruptedException {

			Path[] cacheFilesLocal = DistributedCache.getLocalCacheFiles(context.getConfiguration());

			for (Path eachPath : cacheFilesLocal) {
				System.out.print(eachPath.getParent().toString()+eachPath.getName());
				if (eachPath.getName().toString().trim().equals(regioni_txt)) {
					context.getCounter(MYCOUNTER.FILE_EXISTS).increment(1);
					loadRegioniHashMap(eachPath, context);
				}
			}

		}

		private void loadRegioniHashMap(Path filePath, Context context)
				throws IOException {

			String strLineRead = "";

			try {
				brReader = new BufferedReader(new FileReader(filePath.toString()));

				// Read each line, split and load to HashMap
				while ((strLineRead = brReader.readLine()) != null) {
					String regioniFieldArray[] = strLineRead.split(",");
					RegionMap.put(regioniFieldArray[2].trim(),
							regioniFieldArray[1].trim());
					context.getCounter(MYCOUNTER.PROV_RECORD_COUNT).increment(1);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				context.getCounter(MYCOUNTER.FILE_NOT_FOUND).increment(1);
			} catch (IOException e) {
				context.getCounter(MYCOUNTER.SOME_OTHER_ERROR).increment(1);
				e.printStackTrace();
			}finally {
				if (brReader != null) {
					brReader.close();

				}

			}
		}

		@Override
		public void map(LongWritable key, Text value, Mapper.Context context)
				throws IOException, InterruptedException {

			if (value.toString().length() > 0) {
				String arrDBUAttributes[] = value.toString().split(",");

				String gestore = arrDBUAttributes[9].trim();
				String cat_abb = arrDBUAttributes[10].trim();
				String cf = arrDBUAttributes[8].trim();
				String prov = arrDBUAttributes[6].trim();
				// 4 column: Gestore, Cat abb, CF, Prov
				String lineValue = gestore+";"+ cf + ";" + prov;
				// OUTPUT: 4 column: Gestore, CF, Prov, Regione
				String lineKey = prov;
				// Scarta quelli senza gestore part[9]

				if(prov.length()==2 && gestore.compareTo("--")!=0 && gestore.compareTo("")!=0 && cf.length()!=0){
					try {
						if(RegionMap.containsKey(prov)) {
							strRegionName = RegionMap.get(prov);
							context.getCounter(MYCOUNTER.REG_FOUND).increment(1);
						}else{
							strRegionName = "NOT-FOUND";
							context.getCounter(MYCOUNTER.REG_NOT_FOUND).increment(1);
						}

						if(strRegionName.compareToIgnoreCase("NOT-FOUND")!=0){
							txtMapOutputKey.set(strRegionName+";"+gestore);
							txtMapOutputValue.set(cf);

							context.getCounter(MYCOUNTER.RECORD_COUNT).increment(1);

							context.write(txtMapOutputKey, txtMapOutputValue);
						}
					} catch(NullPointerException e){
						context.getCounter(MYCOUNTER.NULL_POINTER_EXCEPTION_COUNT).increment(1);
						strRegionName = "NOT-FOUND";
					}
					finally {
					}
				}
			}
			strRegionName = "";
		}
	}
}
