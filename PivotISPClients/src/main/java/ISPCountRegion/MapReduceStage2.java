package ISPCountRegion;

import MainJob.MYCOUNTER;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;

public class MapReduceStage2 {
	public static class DBUProvMapper extends
			Mapper<Object, Text, Text, Text> { //DBU
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String record = value.toString();
			String[] parts = record.split(",");
			String gestore = parts[9].trim();
			String cat_abb = parts[10].trim();
			String cf = parts[8].trim();
			String prov = parts[6].trim();
			// 4 column: Gestore, Cat abb, CF, Prov
			String lineValue = "DBUXY"+";" + gestore+";"+ cf + ";" + prov;
			String lineKey = prov;
			// Scarta quelli senza gestore part[9]
			gestore = gestore.replace("--", "");
			if(lineKey.length()==2 && gestore.compareTo("")!=0 && gestore.compareTo("--")!=0 && cf.length()!=0){
				context.getCounter(MYCOUNTER.DBU_RECORD_COUNT).increment(1);
				context.write(new Text(lineKey), new Text(lineValue));
			}
		}
	}

	public static class DBRegioniMapper extends
			Mapper<Object, Text, Text, Text> { //Regioni
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String record = value.toString();
			String[] parts = record.split(",");
			String prov = parts[2].trim();
			String regione = parts[1].trim();
			// Key: Prov, Value: Regione
			context.getCounter(MYCOUNTER.PROV_RECORD_COUNT).increment(1);
			context.write(new Text(prov), new Text("RGNXY"+";" + regione));
		}
	}

	public static class ProvJoinReducer extends
			Reducer<Text, Text, Text, NullWritable> {
		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			ArrayList<Text> T1 = new ArrayList<Text>();
			for (Text t : values) {
				String value;
				boolean cf = false, prov = false;
				String parts[] = t.toString().split(";");
				if (parts[0].equals("RGNXY")) {
					T1.add(new Text(parts[1]));
				} else {
					// GESTORE
					String gestore = parts[1].trim();
					value = gestore;
					try{
						// CF
						value += ";"+parts[2];
						cf = true;
					}catch(Exception e){
						value += ";";
						context.getCounter(MYCOUNTER.CF_EXCEPTION).increment(1);
					}
					try{
						// PROV
						value += ";"+parts[3];
						prov = true;
					}catch(Exception e){
						value += ";";
						context.getCounter(MYCOUNTER.PROV_EXCEPTION).increment(1);
					}
					for(Text val : T1)
					{
						if(cf && prov && gestore.compareTo("--")!=0) {
							value += ";" + val.toString();
							context.getCounter(MYCOUNTER.PROV_JOIN_COUNT).increment(1);
							context.write(new Text(value), NullWritable.get());
						}
					}
				}
			}
		}
	}
}