package ISPCountRegion;

import MainJob.MYCOUNTER;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MapReduceStage3 {
	public static class JoinMapper extends
			Mapper<Object, Text, Text, Text> { //DBU
		private final static IntWritable one = new IntWritable(1);
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String record = value.toString();
			String[] parts = record.split(";");
			// 4 column: Gestore, CF, Prov, Regione
			String gestore = parts[0].trim();
			String cf = parts[1].trim();
			String prov = parts[2].trim();
			String regione = parts[3].trim();
			Text lineKey = new Text(regione+";"+gestore);
			Text lineValue = new Text(cf);
			context.getCounter(MYCOUNTER.PROV_JOIN_COUNT).increment(1);
			context.write(lineKey, lineValue);
		}
	}

	public static class CountReducer extends Reducer<Text, Text, Text, IntWritable> {
		@Override
		protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			int count = 0;
			for (Text value : values){
				String[] parts = value.toString().split(";");
				count++;
			}
			context.getCounter(MYCOUNTER.REG_ISP_COUNT).increment(1);
			context.write(key, new IntWritable(count));
		}
	}
}