package MapSide;

import MainJob.MYCOUNTER;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class MapReduceStage3 {
	private static String regioni_txt = "regioni_txt";
	public static class MapperMapSideJoinDCacheTextFile extends
			Mapper<LongWritable, Text, Text, Text> {

		private static HashMap<String, String> RegionMap = new HashMap<String, String>();
		private BufferedReader brReader;
		private String strRegionName = "";
		private Text txtMapOutputKey = new Text("");
		private Text txtMapOutputValue = new Text("");

		@Override
		protected void setup(Context context) throws IOException,
				InterruptedException {

			Path[] cacheFilesLocal = DistributedCache.getLocalCacheFiles(context.getConfiguration());

			for (Path eachPath : cacheFilesLocal) {
				System.out.print(eachPath.getParent().toString()+eachPath.getName());
				if (eachPath.getName().toString().trim().equals(regioni_txt)) {
					context.getCounter(MYCOUNTER.FILE_EXISTS).increment(1);
					loadRegioniHashMap(eachPath, context);
				}
			}

		}

		private void loadRegioniHashMap(Path filePath, Context context)
				throws IOException {

			String strLineRead = "";

			try {
				brReader = new BufferedReader(new FileReader(filePath.toString()));

				// Read each line, split and load to HashMap
				while ((strLineRead = brReader.readLine()) != null) {
					String regioniFieldArray[] = strLineRead.split(",");
					RegionMap.put(regioniFieldArray[2].trim(),
							regioniFieldArray[1].trim());
					context.getCounter(MYCOUNTER.PROV_RECORD_COUNT).increment(1);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				context.getCounter(MYCOUNTER.FILE_NOT_FOUND).increment(1);
			} catch (IOException e) {
				context.getCounter(MYCOUNTER.SOME_OTHER_ERROR).increment(1);
				e.printStackTrace();
			}finally {
				if (brReader != null) {
					brReader.close();

				}

			}
		}

		@Override
		public void map(LongWritable key, Text value, Mapper.Context context)
				throws IOException, InterruptedException {

			context.getCounter(MYCOUNTER.RECORD_COUNT).increment(1);

			if (value.toString().length() > 0) {
				String arrMELIBAttributes[] = value.toString().split(";");

				try {
					strRegionName = RegionMap.get(arrMELIBAttributes[2].toString());

					strRegionName = ((strRegionName.equals(null) || strRegionName.equals("")) ? "NOT-FOUND" : strRegionName);

					if(strRegionName.compareToIgnoreCase("NOT-FOUND")!=0){
						txtMapOutputKey.set(strRegionName+";"+arrMELIBAttributes[1].toString());
						txtMapOutputValue.set(arrMELIBAttributes[0].toString());

						context.write(txtMapOutputKey, txtMapOutputValue);
					}
				} catch(NullPointerException e){
					context.getCounter(MYCOUNTER.NULL_POINTER_EXCEPTION_COUNT).increment(1);
					strRegionName = "NOT-FOUND";
				}
				finally {
				}
			}
			strRegionName = "";
		}
	}
}
