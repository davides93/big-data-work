package MainJob;

import AverageAnnualElectrictyBill.MapReduceStage4;
import MapSide.MapReduceStage3;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class ReduceJoin extends Configured implements Tool {
	private static String parent_input_path = "/home/hadoop_home/inputs/csv";
	private static String parent_output_path = "/home/hadoop_home/outputs/csv";
	private static String stage1_path = "stage1";
	private static String stage2_path = "stage2";
	private static String stage3_path = "stage3";
	private static String stage4_path = "stage4";
	private static String stage5_path = "stage5";
	private static String regioni_cache = "regioni_cache";
	private static String regioni_txt = "regioni_txt";
	private static String regioni_path = "regioni";
	private static String melib_path = "melib";
	private static String melib_storico_path = "melib_storico";
	private static String avg_join_path = "avg_join";
	private static String avg_join_group_path = "avg_join_group";
	private static String avg_group_path = "avg_group";

	private Configuration conf;
	private boolean mapside=false;
	private boolean stage2=false;
	private boolean stage3=false;
	private boolean stage4=false;

	private ReduceJoin() throws IOException {
	}


	private void init(){
		conf = this.getConf();
		try{
			if(conf.get("mapside").compareToIgnoreCase("yes")==0)
				this.mapside=true;
		}catch(NullPointerException e){
			this.mapside=false;
		}
		try{
			if(conf.get("stage2").compareToIgnoreCase("yes")==0)
				this.stage2=true;
		}catch(NullPointerException e){
			this.stage2=false;
		}
		try{
			if(conf.get("stage3").compareToIgnoreCase("yes")==0)
				this.stage3=true;
		}catch(NullPointerException e){
			this.stage3=false;
		}
		try{
			if(conf.get("stage4").compareToIgnoreCase("yes")==0)
				this.stage4=true;
		}catch(NullPointerException e){
			this.stage4=false;
		}
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new ReduceJoin(), args);
		System.exit(res);
	}

	@Override
	public int run(String[] args) throws Exception {
		init();
		if(mapside)
			avg_stage3_mapside();
		if(stage2)
			avg_stage2();
		if(stage3)
			avg_stage3();
		if(stage4)
			avg_stage4();
		return 0;
	}

	private int avg_stage2() throws InterruptedException, IOException, ClassNotFoundException {
		Path inputPath = new Path(parent_input_path);
		FileSystem fs = inputPath.getFileSystem(conf);
		// DEPRECATED BUT WORKS
		conf.setBoolean("mapred.output.compress",true);
		conf.set("mapred.output.compression.type", "BLOCK");
		conf.setClass("mapred.map.output.compression.codec", GzipCodec.class, CompressionCodec.class);

		conf.set("mapred.textoutputformat.separator", ";");
		//conf.set("mapreduce.output.textoutputformat.separator",";");
		Job job = new Job(conf, "Reduce-Side JOIN Stage 2: "+melib_path+" with "+melib_storico_path);
		job.setJarByClass(ReduceJoin.class);
		job.setReducerClass(AverageAnnualElectrictyBill.MapReduceStage2.MELIBWithLogsJoinReducer.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(NullWritable.class);

		FileOutputFormat.setCompressOutput(job, true);
		FileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

		MultipleInputs.addInputPath(job, new Path(parent_input_path+"/"+stage2_path+"/"+melib_path), TextInputFormat.class,
				AverageAnnualElectrictyBill.MapReduceStage2.DBMELIBAccountNumberMapper.class);
		MultipleInputs.addInputPath(job, new Path(parent_input_path+"/"+stage2_path+"/"+melib_storico_path), TextInputFormat.class,
				AverageAnnualElectrictyBill.MapReduceStage2.MELIBStoricoAccountNumberMapper.class);

		String composeOutputPath = parent_output_path+"/"+stage3_path+"/"+ avg_join_path;
		Path outputPath = new Path(composeOutputPath);
		FileOutputFormat.setOutputPath(job, outputPath);
		fs.delete(outputPath);

		int success = job.waitForCompletion(true) ? 0 : 1;

		moveSuccess(fs, success, composeOutputPath, stage3_path);
		return success;
	}
	private int avg_stage3() throws InterruptedException, IOException, ClassNotFoundException {
		Path inputPath = new Path(parent_input_path);
		FileSystem fs = inputPath.getFileSystem(conf);
		// DEPRECATED BUT WORKS
		conf.setBoolean("mapred.output.compress",true);
		conf.set("mapred.output.compression.type", "BLOCK");
		conf.setClass("mapred.map.output.compression.codec", GzipCodec.class, CompressionCodec.class);

		conf.set("mapred.textoutputformat.separator", ";");
		//conf.set("mapreduce.output.textoutputformat.separator",";");
		Job job = new Job(conf, "Reduce-Side JOIN Stage 3: "+ avg_join_path);
		job.setJarByClass(ReduceJoin.class);
		job.setReducerClass(AverageAnnualElectrictyBill.MapReduceStage3.ProvJoinReducer.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(NullWritable.class);

		FileOutputFormat.setCompressOutput(job, true);
		FileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

		MultipleInputs.addInputPath(job, new Path(parent_input_path+"/"+stage3_path+"/"+regioni_path), TextInputFormat.class,
				AverageAnnualElectrictyBill.MapReduceStage3.DBRegioniMapper.class);
		MultipleInputs.addInputPath(job, new Path(parent_input_path+"/"+stage3_path+"/"+avg_join_path), TextInputFormat.class,
				AverageAnnualElectrictyBill.MapReduceStage3.DBMELIBProvAMapper.class);

		String composeOutputPath = parent_output_path+"/"+stage4_path+"/"+ avg_group_path;
		Path outputPath = new Path(composeOutputPath);
		FileOutputFormat.setOutputPath(job, outputPath);
		fs.delete(outputPath);

		int success = job.waitForCompletion(true) ? 0 : 1;
		moveSuccess(fs, success, composeOutputPath, stage4_path);
		return success;
	}

	private int avg_stage3_mapside() throws InterruptedException, IOException, ClassNotFoundException, URISyntaxException {
		Path inputPath = new Path(parent_input_path);
		FileSystem fs = inputPath.getFileSystem(conf);
		// DEPRECATED BUT WORKS
		conf.set("mapred.textoutputformat.separator", ";");
		//conf.set("mapreduce.output.textoutputformat.separator",";");

		DistributedCache.addCacheFile(new URI(parent_input_path+"/"+stage3_path+"/"+regioni_cache+"/"+regioni_txt), conf);

		Job job = new Job(conf, "Map-side JOIN with text lookup file in DCache - Stage 3: "+ avg_join_path);

		job.setJarByClass(ReduceJoin.class);
		job.setReducerClass(MapReduceStage4.AverageReducer.class);
		job.setMapperClass(MapReduceStage3.MapperMapSideJoinDCacheTextFile.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(parent_input_path+"/"+stage3_path+"/"+avg_join_path));

		String composeOutputPath = parent_output_path+"/"+stage5_path+"/"+ avg_join_group_path;
		Path outputPath = new Path(composeOutputPath);
		FileOutputFormat.setOutputPath(job, outputPath);
		fs.delete(outputPath);

		return job.waitForCompletion(true) ? 0 : 1;
	}

	private int avg_stage4() throws InterruptedException, IOException, ClassNotFoundException {
		Path inputPath = new Path(parent_input_path);
		FileSystem fs = inputPath.getFileSystem(conf);
		// DEPRECATED BUT WORKS
		conf.set("mapred.textoutputformat.separator", ";");
		//conf.set("mapreduce.output.textoutputformat.separator",";");
		Job job = new Job(conf, "Reduce-Side GROUP BY Stage 4: "+ avg_join_path);
		job.setJarByClass(ReduceJoin.class);
		job.setMapperClass(MapReduceStage4.JoinMapper.class);
		job.setReducerClass(MapReduceStage4.AverageReducer.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(parent_input_path+"/"+stage4_path+"/"+ avg_group_path));
		Path outputPath = new Path(parent_output_path+"/"+stage5_path+"/"+ avg_group_path);
		FileOutputFormat.setOutputPath(job, outputPath);
		fs.delete(outputPath);

		return job.waitForCompletion(true) ? 0 : 1;
	}

	private static void moveSuccess(FileSystem fs, int success, String composeOutputPath, String stage_path) throws IOException {
		FileStatus[] files = fs.listStatus(new Path(composeOutputPath));
		String fileName = "";
		boolean found = false;
		for (FileStatus status : files){
			if(status.getPath().getName().contains("part")){
				found = true;
				fileName = status.getPath().getParent().getName();
			}
		}
		if(success==0 && found){
			String successFile = composeOutputPath+"/_SUCCESS";
			if(fs.exists(new Path(successFile))) {
				fs.delete(new Path(successFile), true);
				fs.rename(new Path(composeOutputPath),new Path(parent_input_path+"/"+stage_path+"/"+fileName));
			} else {
				throw new IOException("No file _SUCCESS found");
			}
			//FileUtil.fullyDelete(new File(successFile));
		}else{
			System.exit(-1);
		}
	}
}
