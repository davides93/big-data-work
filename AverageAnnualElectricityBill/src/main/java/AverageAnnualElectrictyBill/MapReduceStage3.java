package AverageAnnualElectrictyBill;

import MainJob.MYCOUNTER;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;

public class MapReduceStage3 {
	public static class DBMELIBProvAMapper extends
			Mapper<Object, Text, Text, Text> {
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String record = value.toString();
			String[] parts = record.split(";");
			//f_totale, trader, provincia
			String trader = parts[1].trim();
			String provincia = parts[2].trim();
			String f_totale = parts[0].trim();

			String lineValue = "MELIB"+";"+f_totale+";"+trader;
			context.write(new Text(provincia), new Text(lineValue));
			context.getCounter(MYCOUNTER.MELIB_RECORD_COUNT).increment(1);
		}
	}

	public static class DBRegioniMapper extends
			Mapper<Object, Text, Text, Text> { //Regioni
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String record = value.toString();
			String[] parts = record.split(",");
			// Key: Prov, Value: Regione
			context.write(new Text(parts[2].trim()), new Text("RGNXY"+";" + parts[1].trim()));
			context.getCounter(MYCOUNTER.PROV_RECORD_COUNT).increment(1);
		}
	}

	public static class ProvJoinReducer extends
			Reducer<Text, Text, Text, NullWritable> {
		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			ArrayList<Text> T1 = new ArrayList<Text>();
			context.getCounter(MYCOUNTER.PROV_JOIN_COUNT).increment(1);
			for (Text t : values) {
				String value;
				String parts[] = t.toString().split(";");
				if (parts[0].equals("RGNXY")) {
					T1.add(new Text(parts[1]));
				} else {
					String f_totale = parts[1];
					String trader = parts[2];
					// trader, f_totale, Regione
					value = trader+";"+f_totale;
					for(Text val : T1)
					{
						value += ";"+val.toString();
						context.write(new Text(value), NullWritable.get());
					}
				}
			}
		}
	}
}
