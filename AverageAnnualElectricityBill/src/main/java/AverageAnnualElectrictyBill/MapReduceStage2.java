package AverageAnnualElectrictyBill;

import MainJob.MYCOUNTER;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;

public class MapReduceStage2 {
	public static class DBMELIBAccountNumberMapper extends
			Mapper<Object, Text, Text, Text> {
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String record = value.toString();
			String[] parts = record.split(",");
			String account_number = parts[1].trim();
			String trader = parts[11].trim();
			String provincia = parts[18].trim();
			String piva = parts[9].trim();
			String cf = parts[10].trim();

			if(account_number.length()!=0 && trader.length()!=0 && provincia.length()!=0){
				String lineValue = "MELIB"+";" + trader+";"+provincia;
				context.write(new Text(account_number), new Text(lineValue));
				context.getCounter(MYCOUNTER.MELIB_ACCOUNT_NUMBER_RECORD_COUNT).increment(1);
			}
		}
	}

	public static class MELIBStoricoAccountNumberMapper extends
			Mapper<Object, Text, Text, Text> {
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String record = value.toString();
			String[] parts = record.split(",");
			// 4 column: CF
			String account_number = parts[0].trim();
			try{
				Integer f1 = Integer.parseInt(parts[4].trim());
				Integer f2 = Integer.parseInt(parts[7].trim());
				Integer f3 = Integer.parseInt(parts[10].trim());
				Integer f_totale = f1+f2+f3;
				if(account_number.length()!=0){
					String lineValue = "MLB_STORICO"+";" + f_totale;
					context.write(new Text(account_number), new Text(lineValue));
					context.getCounter(MYCOUNTER.MELIB_STORICO_ACCOUNT_NUMBER_RECORD_COUNT).increment(1);
				}
			}catch (NumberFormatException e){
				//SKip
			}
		}
	}

	public static class MELIBWithLogsJoinReducer extends
			Reducer<Text, Text, Text, NullWritable> {
		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			ArrayList<Text> T1 = new ArrayList<>();
			context.getCounter(MYCOUNTER.JOIN_MELIB_ACCOUNT_NUMBER_RECORD_COUNT).increment(1);
			for (Text t : values) {
				String value;
				String parts[] = t.toString().split(";");
				if (parts[0].equals("MELIB")) {
					String composeMelib = parts[1].trim();
					composeMelib += ";"+parts[2].trim();
					T1.add(new Text(composeMelib));
				} else {
					value = parts[1].trim();
					for(Text val : T1)
					{
						value += ";"+val.toString();
						//f_totale, trader, provincia
						context.write(new Text(value), NullWritable.get());
					}
				}
			}
		}
	}
}
