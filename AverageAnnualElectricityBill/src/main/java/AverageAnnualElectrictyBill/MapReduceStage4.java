package AverageAnnualElectrictyBill;

import MainJob.MYCOUNTER;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MapReduceStage4 {
	public static class JoinMapper extends
			Mapper<Object, Text, Text, Text> { //DBU
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String record = value.toString();
			String[] parts = record.split(";");
			// trader, f_totale, Regione
			Text lineKey = new Text(parts[2].trim()+";"+parts[0].trim());
			context.write(lineKey, new Text(parts[1]));
			context.getCounter(MYCOUNTER.RECORD_COUNT).increment(1);
		}
	}

	public static class AverageReducer extends Reducer<Text, Text, Text, Text> {
		@Override
		protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			int count = 0;
			double total = 0.0;
			context.getCounter(MYCOUNTER.REG_TRAD_COUNT).increment(1);
			for (Text value : values){
				count++;
				total += Integer.parseInt(value.toString());
			}
			double average = total / count;
			context.write(key, new Text(String.valueOf(average)));
		}
	}
}